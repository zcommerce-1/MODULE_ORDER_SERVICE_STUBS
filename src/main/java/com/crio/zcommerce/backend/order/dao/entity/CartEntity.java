package com.crio.zcommerce.backend.order.dao.entity;

import com.crio.zcommerce.backend.order.model.ItemQuantity;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import java.util.List;
import java.util.Objects;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(
    name = "cart",
    uniqueConstraints = {
      @UniqueConstraint(columnNames = "id"),
      @UniqueConstraint(columnNames = "userId")
    },
    indexes = @Index(name = "IDX_userId", columnList = "userId"))
public class CartEntity extends BaseEntity {

  @Column(nullable = false)
  private String userId;

  @Column(nullable = false)
  @ElementCollection
  @CollectionTable(name = "cart_item_details", joinColumns = @JoinColumn(name = "cart_id"))
  private List<ItemQuantity> itemQuantityList;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    CartEntity that = (CartEntity) o;
    return getId() != null && Objects.equals(getId(), that.getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}

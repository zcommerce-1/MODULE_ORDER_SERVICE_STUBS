package com.crio.zcommerce.backend.order.utils.kafka.model;

/**
 * This interface represents an event object used for sending data to Kafka topics.
 * Implementing classes can provide more customized data by overriding the getUserId() method.
 * The getUserId() method returns the user ID associated with the event.
 *
 * Note: The default implementation of getUserId() returns null.
 * Implementing classes should override this method to provide a specific user ID value if
 * applicable.
 *
 * <p>This interface serves as a contract for event objects that are sent to Kafka topics. It
 * allows for flexibility
 * in defining custom event classes while ensuring that the user ID can be retrieved consistently
 * across different events.
 */
public interface ConsumerEvent {
  default String getUserId() {
    return null;
  }
}

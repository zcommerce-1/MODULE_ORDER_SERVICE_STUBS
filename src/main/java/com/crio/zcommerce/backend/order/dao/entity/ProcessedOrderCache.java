package com.crio.zcommerce.backend.order.dao.entity;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;

@Data
@RequiredArgsConstructor
@RedisHash("ProcessedOrders")
public class ProcessedOrderCache {
    @NonNull
    @Id
    private String idempotentToken;

    @TimeToLive
    private Long expiration;
}

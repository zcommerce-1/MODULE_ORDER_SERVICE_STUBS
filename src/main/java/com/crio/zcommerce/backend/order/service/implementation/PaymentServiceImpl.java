package com.crio.zcommerce.backend.order.service.implementation;

import com.crio.zcommerce.backend.order.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.order.common.exceptions.ForbiddenException;
import com.crio.zcommerce.backend.order.common.exceptions.PaymentAlreadyMadeException;
import com.crio.zcommerce.backend.order.controller.exchange.requests.InitiatePaymentRequest;
import com.crio.zcommerce.backend.order.controller.exchange.requests.UpdateOrderStatusRequest;
import com.crio.zcommerce.backend.order.controller.exchange.requests.UpdatePaymentRequest;
import com.crio.zcommerce.backend.order.dao.OrderDao;
import com.crio.zcommerce.backend.order.dao.PaymentDao;
import com.crio.zcommerce.backend.order.model.Order;
import com.crio.zcommerce.backend.order.model.Payment;
import com.crio.zcommerce.backend.order.model.enums.OrderStatus;
import com.crio.zcommerce.backend.order.model.enums.PaymentStatus;
import com.crio.zcommerce.backend.order.service.PaymentService;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {

  private final OrderDao orderDao;

  private final PaymentDao paymentDao;

  @Override
  public Payment getPaymentDetails(Long paymentId, String userId)
      throws EntityDoesNotExistException, ForbiddenException {
    String logPrefix = "event: getPaymentDetails, message: ";
    Optional<Payment> paymentOptional = paymentDao.findByPaymentId(paymentId);

    if (paymentOptional.isPresent()) {
      Payment payment = paymentOptional.get();
      if (!payment.getOrder().getUserId().equals(userId)) {
        throw new ForbiddenException(
            String.format(
                "%s paymentId: %s do not belong to userId : %s ", logPrefix, paymentId, userId));
      }
      return payment;
    } else {
      // throwing entity not found exception as the order is not found
      log.info(logPrefix + "payment details not found for the given paymentId : {}", paymentId);
      throw new EntityDoesNotExistException(
          String.format(
              "%s payment details not found for the given paymentId : %s", logPrefix, paymentId));
    }
  }

  @Override
  public Payment initiatePayment(InitiatePaymentRequest request)
      throws EntityDoesNotExistException {
    String logPrefix = "event : initiatePayment, message: ";

    // fetching order details
    Optional<Order> orderOptional =
        orderDao.findByUserIdAndOrderId(request.getUserId(), request.getOrderId());

    Order order =
        orderOptional.orElseThrow(
            () ->
                new EntityDoesNotExistException(
                    String.format(
                        "%s order details not found for the given userId : %s and orderId : %s",
                        logPrefix, request.getUserId(), request.getOrderId())));

    // check if prior successful payments are present. If yes, don't allow payment again.
    order.getPaymentList().stream()
        .filter(payment -> payment.getPaymentStatus().equals(PaymentStatus.COMPLETED))
        .findFirst()
        .ifPresent(
            payment -> {
              String errorMessage = "Payment already made for orderId: " + request.getOrderId();
              log.info(logPrefix + errorMessage);
              throw new PaymentAlreadyMadeException(errorMessage);
            });

    // todo check what is the payment status if payment type is COD
    // creating payment with payment initiated status
    Payment payment =
        Payment.builder()
            .order(order)
            .paymentType(request.getPaymentType())
            .paymentGateway(request.getPaymentGateway())
            .paymentStatus(PaymentStatus.INITIATED)
            .build();

    // creating a payment initiated entry into the payment table
    Payment initiatedPayment = paymentDao.createPayment(payment);
    log.info(
        logPrefix
            + "payment has been initiated for "
            + "userId : {} , orderId : {} , paymentType : {} , paymentGateway : {}",
        request.getUserId(),
        request.getOrderId(),
        request.getPaymentType(),
        request.getPaymentGateway());
    return initiatedPayment;
  }

  @Override
  public Payment updatePayment(Long paymentId, UpdatePaymentRequest request)
      throws EntityDoesNotExistException {
    String logPrefix = "event : updatePayment, message: ";
    // todo check if there any validation required here - state management validation
    Payment updatedPayment = paymentDao.updatePayment(paymentId, request);
    log.info(
        logPrefix
            + "payment has been updated for paymentId : {} , "
            + " : {} , paymentType : {} , paymentGateway : {} , paymentStatus : {}",
        updatedPayment.getId(),
        updatedPayment.getOrder().getId(),
        updatedPayment.getPaymentType(),
        updatedPayment.getPaymentGateway(),
        updatedPayment.getPaymentStatus());

    // check if payment is marked as completed, if so change order status to IN_PROGRESS
    if (updatedPayment.getPaymentStatus().equals(PaymentStatus.COMPLETED)) {
      UpdateOrderStatusRequest updateOrderStatusRequest =
          UpdateOrderStatusRequest.builder().orderStatus(OrderStatus.IN_PROGRESS).build();
      orderDao.updateOrderStatus(updatedPayment.getOrder().getId(), updateOrderStatusRequest);
    }
    return updatedPayment;
  }

  @Override
  public void deletePayment(Long paymentId) {
    String logPrefix = "event: deletePayment, message: ";
    paymentDao.deletePayment(paymentId);
    log.info(logPrefix + "payment deleted for paymentId : {}", paymentId);
  }
}

package com.crio.zcommerce.backend.order.dao.repository;

import com.crio.zcommerce.backend.order.dao.entity.CartEntity;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<CartEntity, Long> {

  Optional<CartEntity> findByUserId(String userId);
}

package com.crio.zcommerce.backend.order.mapper;

import com.crio.zcommerce.backend.order.dao.entity.PaymentEntity;
import com.crio.zcommerce.backend.order.model.Payment;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface PaymentMapper {

  @Mapping(target = "order", ignore = true)
  Payment toDto(PaymentEntity paymentEntity);

  PaymentEntity toEntity(Payment payment);

  @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
  PaymentEntity updateEntity(Payment payment, @MappingTarget PaymentEntity paymentEntity);
}

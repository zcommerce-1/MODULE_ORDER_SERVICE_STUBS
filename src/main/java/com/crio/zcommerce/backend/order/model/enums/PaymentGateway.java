package com.crio.zcommerce.backend.order.model.enums;

public enum PaymentGateway {
  // todo add comments - doc string
  STRIPE,

  UPI,

  PAYPAL,

  NONE
}

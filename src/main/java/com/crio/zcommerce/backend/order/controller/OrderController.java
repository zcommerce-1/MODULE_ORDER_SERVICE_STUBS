package com.crio.zcommerce.backend.order.controller;

import com.crio.zcommerce.backend.order.common.api.ApiResponse;
import com.crio.zcommerce.backend.order.common.api.ApiResponseStatus;
import com.crio.zcommerce.backend.order.common.exceptions.*;
import com.crio.zcommerce.backend.order.controller.exchange.requests.CreateOrderRequest;
import com.crio.zcommerce.backend.order.controller.exchange.requests.UpdateOrderStatusRequest;
import com.crio.zcommerce.backend.order.model.Order;
import com.crio.zcommerce.backend.order.model.OrderItems;
import com.crio.zcommerce.backend.order.service.OrderService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.metrics.annotation.Timed;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;

@Timed
@RestController
@RequiredArgsConstructor
@RequestMapping(OrderController.ORDER_API_PREFIX)
public class OrderController {

  private final OrderService orderService;

  public static final String ORDER_API_PREFIX = "/api/v1/order";
  public static final String ID_ENDPOINT = "/{id}";
  public static final String ALL_ENDPOINT = "/all";

  public static final String ORDER_STATUS_API_PREFIX = "/status";

  /**
   * URL: /api/v1/order/all
   * Description: Get all orders of the user.
   * Method: GET
   *
   * @return Page of order
   */
  @GetMapping(ALL_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_USER')")
  public ResponseEntity<Page<OrderItems>> getAllOrders(
      @SortDefault(sort = "id") @PageableDefault(size = 20) Pageable pageable,
      Principal principal) {

    String userId = principal.getName();
    return ResponseEntity.status(HttpStatus.OK).body(orderService.getAllOrders(userId, pageable));
  }

  /**
   * URL: /api/v1/order/{id}
   * Description: Get order details of the id
   * Method: GET
   *
   * @param userId of type String
   * @return Order
   */
  @GetMapping(ID_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_USER')")
  public ResponseEntity<ApiResponse<OrderItems>> getOrderById(
      @PathVariable("id") Long orderId, Principal principal) {

    String userId = principal.getName();
    ApiResponse<OrderItems> response = new ApiResponse<>();
    try {
      OrderItems order = orderService.getPopulatedOrderDetails(userId, orderId);
      response.setData(order);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(
          String.format("order Details not found for the given orderId : %s", orderId));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    } catch (ApiException | IOException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/order
   * Description: Create order
   * Method: POST
   *
   * @param createOrderRequest of type CreateOrderRequest
   * @return Order
   */
  @PostMapping
  @PreAuthorize("hasAuthority('ROLE_USER')")
  public ResponseEntity<ApiResponse<Order>> createOrder(
      @RequestBody @Valid CreateOrderRequest createOrderRequest,
      Principal principal,
      @RequestHeader(value = "Idempotency-Token", required = false) String idempotencyHeader) {

    createOrderRequest.setUserId(principal.getName());
    ApiResponse<Order> response = new ApiResponse<>();
    try {
      Order order;
if (StringUtils.isEmpty(idempotencyHeader)) {  response.setStatus(ApiResponseStatus.FAILED);  response.setMessage("Missing required header: Idempotency-Token");  return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);}order = orderService.createOrderIdempotent(createOrderRequest, idempotencyHeader);
      response.setData(order);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (InvalidUserException | IOException | ApiException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    } catch (OutOfStockException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    } catch (DuplicateRequestException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
    }
  }

  /**
   * URL: /api/v1/order/{id}/status
   * Description: Update order status
   * Method: PATCH
   *
   * @param updateOrderStatusRequest of type UpdateOrderStatusRequest
   * @return Order
   */
  @PatchMapping(ID_ENDPOINT + ORDER_STATUS_API_PREFIX)
  @PreAuthorize("hasAuthority('ROLE_USER')")
  public ResponseEntity<ApiResponse<Order>> updateOrderStatus(
      @PathVariable("id") Long orderId,
      @RequestBody @Valid UpdateOrderStatusRequest updateOrderStatusRequest,
      Principal principal) {

    String userId = principal.getName();
    ApiResponse<Order> response = new ApiResponse<>();
    try {
      Order order = orderService.updateOrderStatus(orderId, userId, updateOrderStatusRequest);
      response.setData(order);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(
          String.format(
              "Update Order failed as order Details not found for the given orderId : %s",
              orderId));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    } catch (ForbiddenException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(
          String.format(
              "Update Order failed as orderId: %s do not belong to userId: %s", orderId, userId));
      return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
    }
  }

  /**
   * URL: /api/v1/order/{id}
   * Description: Delete order of given order id
   * Method: DELETE
   */
  @DeleteMapping(ID_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  public ResponseEntity<ApiResponse<String>> deleteOrder(@PathVariable("id") Long orderId) {
    orderService.deleteOrder(orderId);
    ApiResponse<String> response = new ApiResponse<>();
    response.setStatus(ApiResponseStatus.SUCCESS);
    response.setMessage("Order deleted");
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }
}

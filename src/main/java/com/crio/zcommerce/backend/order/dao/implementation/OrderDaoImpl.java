package com.crio.zcommerce.backend.order.dao.implementation;

import com.crio.zcommerce.backend.order.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.order.controller.exchange.requests.UpdateOrderStatusRequest;
import com.crio.zcommerce.backend.order.dao.OrderDao;
import com.crio.zcommerce.backend.order.dao.entity.OrderEntity;
import com.crio.zcommerce.backend.order.dao.repository.OrderRepository;
import com.crio.zcommerce.backend.order.mapper.OrderMapper;
import com.crio.zcommerce.backend.order.model.Order;
import java.time.ZonedDateTime;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
@RequiredArgsConstructor
public class OrderDaoImpl implements OrderDao {

  private final OrderRepository orderRepository;

  private final OrderMapper orderMapper;

  @Override
  @Transactional
  public Optional<Order> findByUserIdAndOrderId(String userId, Long orderId) {
    // fetching for order with userId and orderId
    Optional<OrderEntity> orderEntityOptional = orderRepository.findByIdAndUserId(orderId, userId);

    // check if order details is present
    if (orderEntityOptional.isPresent()) {
      Order order = orderMapper.toDto(orderEntityOptional.get());
      return Optional.of(order);
    }

    // returning empty optional as order details is not present
    return Optional.empty();
  }

  @Override
  @Transactional
  public Page<Order> findAllByUserIdSortedByDateDesc(String userId, Pageable pageable) {
    // fetch for all order for a user ordered by date desc
    Page<OrderEntity> orderEntityPage =
        orderRepository.findAllByUserIdOrderByCreatedAtDesc(userId, pageable);

    // converting page of order entity to page of order dto and returned
    return orderEntityPage.map(orderEntity -> orderMapper.toDto(orderEntity));
  }

  @Override
  public Page<Order> findAllByUserIdAndCreatedAtGreaterThanEqual(
      String userId, ZonedDateTime startDateTime, Pageable pageable) {
    // fetch for all order for a user ordered by date desc
    Page<OrderEntity> orderEntityPage =
        orderRepository.findAllByUserIdAndCreatedAtGreaterThanEqual(
            userId, startDateTime, pageable);

    // converting page of order entity to page of order dto and returned
    return orderEntityPage.map(orderEntity -> orderMapper.toDto(orderEntity));
  }

  @Override
  public Order createOrder(Order order) {
    OrderEntity orderEntity = orderMapper.toEntity(order);

    // inserting order entity into the order table
    OrderEntity orderEntity1 = orderRepository.save(orderEntity);
    return orderMapper.toDto(orderEntity1);
  }

  @Override
  @Transactional
  public Order updateOrderStatus(Long orderId, UpdateOrderStatusRequest request)
      throws EntityDoesNotExistException {
    String logPrefix = "event: updateOrderStatus, message: ";
    // fetching the existing order entity for the orderId
    Optional<OrderEntity> existingOrderEntityOptional = orderRepository.findById(orderId);

    if (!existingOrderEntityOptional.isPresent()) {
      log.info(logPrefix + "order details not found for the orderId : {}", orderId);
      throw new EntityDoesNotExistException(
          String.format(
              "%s update failed as order details not found for the orderId : %s",
              logPrefix, orderId));
    }

    // checking if its valid order status update
    // todo check for the order status state management check - idempotency

    // updating the order status in the entity
    OrderEntity existingOrderEntity = existingOrderEntityOptional.get();
    existingOrderEntity.setStatus(request.getOrderStatus());
    OrderEntity updatedOrderEntity = orderRepository.save(existingOrderEntity);
    return orderMapper.toDto(updatedOrderEntity);
  }

  @Override
  @Transactional
  public void deleteOrder(Long orderId) {
    orderRepository.deleteById(orderId);
  }
}

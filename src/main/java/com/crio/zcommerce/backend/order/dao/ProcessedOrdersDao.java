package com.crio.zcommerce.backend.order.dao;

public interface ProcessedOrdersDao {
    void create(String idempotencyToken);

    boolean exists(String idempotencyToken);
}

package com.crio.zcommerce.backend.order.model;

import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public abstract class BaseModel {

  protected Long id;
  protected ZonedDateTime createdAt;
  protected ZonedDateTime updatedAt;
  protected Long version;
}

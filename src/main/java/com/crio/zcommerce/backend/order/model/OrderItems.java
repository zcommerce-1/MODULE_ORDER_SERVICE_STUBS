package com.crio.zcommerce.backend.order.model;

import com.crio.zcommerce.backend.order.model.enums.OrderStatus;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class OrderItems extends BaseModel {

  private Long id;

  @NotNull private String userId;

  @NotNull @NotEmpty private List<ItemDetailsQuantity> itemDetailsQuantity;

  @NotNull private OrderStatus orderStatus;

  @NotNull private Double amount;

  private Set<Payment> paymentList;

  private Tracking tracking;

  // note: below fields should ideally go to diff table under a user,
  // but for the simplicity we have kept here
  @NotNull private String shippingAddress;

  @NotNull private String billingAddress;
}

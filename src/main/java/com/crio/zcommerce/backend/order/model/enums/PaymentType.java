package com.crio.zcommerce.backend.order.model.enums;

public enum PaymentType {
  // todo add comments - doc string
  CASH_ON_DELIVERY,

  DEBIT_CARD,

  CREDIT_CARD,

  UPI,

  NET_BANKING
}

package com.crio.zcommerce.backend.order.utils.kafka;

import com.crio.zcommerce.backend.order.utils.kafka.model.ConsumerEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class KafkaUtil {

  private final KafkaTemplate kafkaTemplate;

  private final ObjectMapper objectMapper;

  @Async
  public void sendEvent(ConsumerEvent consumerEvent, String topic) {
    try {
      try {
        String key = consumerEvent.getUserId();
        log.info("Sending consumer event: {} to topic: {} with key: {}", consumerEvent, topic, key);
        ProducerRecord<String, String> message =
            new ProducerRecord<>(topic, key, objectMapper.writeValueAsString(consumerEvent));

        kafkaTemplate.send(message);

      } catch (Exception e) {
        log.error(
            "Error while sending event with key, "
                + "consumerEvent: {}, topic: {}, exception: {}, "
                + "Sending event without key",
            consumerEvent,
            topic,
            e.getMessage());
        kafkaTemplate.send(topic, objectMapper.writeValueAsString(consumerEvent));
      }

      log.info("consumer event: {} to topic: {} sent successfully", consumerEvent, topic);

    } catch (JsonProcessingException jpe) {
      log.error(
          "Error while sending event, consumerEvent: {}, topic: {}, exception: {}",
          consumerEvent,
          topic,
          jpe.getMessage());
    }
  }
}

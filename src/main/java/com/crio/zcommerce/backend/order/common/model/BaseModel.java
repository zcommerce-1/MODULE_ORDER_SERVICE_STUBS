package com.crio.zcommerce.backend.order.common.model;

import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class BaseModel {

  protected Long id;
  protected ZonedDateTime createdAt;
  protected ZonedDateTime updatedAt;
  protected Long version;
}

package com.crio.zcommerce.backend.order.config.security;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Log4j2
@Configuration
@ConfigurationProperties(prefix = "com.crio.zcommerce.backend.order.config.security.cognito")
public class CognitoConfig {

  private String issuerUri;
}

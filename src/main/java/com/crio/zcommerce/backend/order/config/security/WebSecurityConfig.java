package com.crio.zcommerce.backend.order.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.oauth2.server.resource.web.authentication.BearerTokenAuthenticationFilter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig {

  @Autowired
  @Qualifier("requestMappingHandlerMapping")
  private RequestMappingHandlerMapping handlerMapping;

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http, CognitoConfig cognitoConfig)
      throws Exception {
    CorsConfiguration corsConfiguration = new CorsConfiguration();
    corsConfiguration.addAllowedOrigin("*");
    corsConfiguration.addAllowedHeader("*");
    corsConfiguration.addAllowedMethod("*");
    corsConfiguration.setAllowCredentials(false);

    UrlBasedCorsConfigurationSource configurationSource = new UrlBasedCorsConfigurationSource();
    configurationSource.registerCorsConfiguration("/**", corsConfiguration);

    // todo: Enable Csrf
    http.csrf(AbstractHttpConfigurer::disable)
            .authorizeHttpRequests(
                    auth ->
                            auth.requestMatchers("/swagger-ui/**", "/v3/api-docs/**")
                                    .permitAll()
                                    .requestMatchers("/public/**", "/error")
                                    .permitAll() // Allow access to any URL starting with /public
                                    .anyRequest()
                                    .authenticated() // All other URLs require authentication
            )
            .cors(customizer -> customizer.configurationSource(configurationSource))
            .addFilterBefore(
                    new CognitoTokenFilter(cognitoConfig), BearerTokenAuthenticationFilter.class)
            .exceptionHandling(configurer -> {
              configurer.authenticationEntryPoint(new AuthenticationExceptionEntryPoint(handlerMapping));
            });
    return http.build();
  }
}

package com.crio.zcommerce.backend.order.utils.kafka.model;

import com.crio.zcommerce.backend.order.model.Order;
import com.crio.zcommerce.backend.order.model.enums.WebhookTrigger;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WebhookEvent<T> implements ConsumerEvent {
    String userId;
    @NotNull
    private T payload;
    private String trigger;
}

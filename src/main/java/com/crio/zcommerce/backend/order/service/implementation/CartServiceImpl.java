package com.crio.zcommerce.backend.order.service.implementation;

import com.crio.zcommerce.backend.order.clients.ProductClient;
import com.crio.zcommerce.backend.order.common.exceptions.ApiException;
import com.crio.zcommerce.backend.order.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.order.common.model.Item;
import com.crio.zcommerce.backend.order.controller.exchange.responses.CartResponse;
import com.crio.zcommerce.backend.order.dao.CartDao;
import com.crio.zcommerce.backend.order.model.Cart;
import com.crio.zcommerce.backend.order.model.ItemDetailsQuantity;
import com.crio.zcommerce.backend.order.model.ItemQuantity;
import com.crio.zcommerce.backend.order.service.CartService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {

  private final CartDao cartDao;

  private final ProductClient productClient;

  private final ModelMapper modelMapper;

  @Override
  public Cart getCartDetails(String userId) throws EntityDoesNotExistException {
    String logPrefix = "event: getCartDetails, message: ";
    Optional<Cart> cartOptional = cartDao.findByUserId(userId);
    if (cartOptional.isPresent()) {
      return cartOptional.get();
    } else {
      log.info(logPrefix + "cart details not found for the userId : {}", userId);
      throw new EntityDoesNotExistException(
          String.format("%s cart details not found for the userId : %s", logPrefix, userId));
    }
  }

  @Override
  public CartResponse getUserCartDetails(String userId)
      throws EntityDoesNotExistException, IOException, ApiException {
    String logPrefix = "event: getUserCartDetails, message: ";
    Optional<Cart> cartOptional = cartDao.findByUserId(userId);
    if (!cartOptional.isPresent()) {
      log.info(logPrefix + "cart details not found for the userId : {}", userId);
      throw new EntityDoesNotExistException(
          String.format("cart details not found for the userId : %s", userId));
    }

    Cart cart = cartOptional.get();
    List<ItemDetailsQuantity> availableItems = new ArrayList<>();
    List<ItemDetailsQuantity> outOfStockItems = new ArrayList<>();

    populateItems(cart, availableItems, outOfStockItems);

    CartResponse cartResponse = modelMapper.map(cart, CartResponse.class);
    cartResponse.setUserId(cart.getUserId());
    cartResponse.setAvailableItem(availableItems);
    cartResponse.setOutOfStockItem(outOfStockItems);

    return cartResponse;
  }

  private void populateItems(
      Cart cart,
      List<ItemDetailsQuantity> availableItems,
      List<ItemDetailsQuantity> outOfStockItems)
      throws IOException, ApiException {
    for (ItemQuantity itemQuantity : cart.getItemQuantityList()) {
      Item item = productClient.getProductDetails(itemQuantity.getItemId());
      Integer itemInventoryQuantity = item.getInventory().getQuantityInStock();
      Integer itemCartQuantity = itemQuantity.getQuantity();

      ItemDetailsQuantity.ItemDetailsQuantityBuilder builder =
          ItemDetailsQuantity.builder().item(item);
      if (itemInventoryQuantity >= itemCartQuantity) {
        availableItems.add(builder.quantity(itemCartQuantity).build());
      } else {
        availableItems.add(builder.quantity(itemInventoryQuantity).build());
        outOfStockItems.add(builder.quantity(itemCartQuantity - itemInventoryQuantity).build());
      }
    }
  }

  @Override
  public Cart createCart(Cart cart) {
    return cartDao.createCart(cart);
  }

  @Override
  public Cart updateCart(Long cartId, Cart cart) throws EntityDoesNotExistException {
    return cartDao.updateCart(cartId, cart);
  }

  @Override
  public Cart updateItemInCart(String userId, ItemQuantity itemQuantity)
      throws EntityDoesNotExistException, ApiException, IOException {
    String logPrefix = "event: updateItemInCart, message: ";

    Cart cart =
        cartDao
            .findByUserId(userId)
            .orElseGet(
                () ->
                    cartDao.createCart(
                        Cart.builder()
                            .userId(userId)
                            .itemQuantityList(Collections.emptyList())
                            .build()));

    if (!checkStock(itemQuantity)) {
      log.info(logPrefix + "inventory exhausted for the itemId : {}", itemQuantity.getItemId());
      throw new EntityDoesNotExistException(
          String.format("inventory exhausted for the itemId : {}", itemQuantity.getItemId()));
    }

    Optional<ItemQuantity> itemQuatityOptionalFromCart =
        getItemFromCart(cart, itemQuantity.getItemId());
    if (itemQuatityOptionalFromCart.isPresent()) {
      cart.getItemQuantityList().remove(itemQuatityOptionalFromCart.get());
    }
    if (itemQuantity.getQuantity() > 0) {
      cart.getItemQuantityList().add(itemQuantity);
    }
    Cart updatedCart = cartDao.updateCart(cart.getId(), cart);
    return updatedCart;
  }

  private Optional<ItemQuantity> getItemFromCart(Cart cart, Long itemId) {
    for (ItemQuantity itemQuantity : cart.getItemQuantityList()) {
      if (itemQuantity.getItemId().equals(itemId)) {
        return Optional.of(itemQuantity);
      }
    }
    return Optional.empty();
  }

  private Boolean checkStock(ItemQuantity itemQuantity) throws IOException, ApiException {
    Item item = productClient.getProductDetails(itemQuantity.getItemId());
    if (item.getInventory().getQuantityInStock() < itemQuantity.getQuantity()) {
      return false;
    }
    return true;
  }

  @Override
  public void deleteCart(Long cartId) {
    cartDao.deleteCart(cartId);
  }
}

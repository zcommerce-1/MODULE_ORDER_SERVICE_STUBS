package com.crio.zcommerce.backend.order.model.enums;

public enum TrackingStatus {
  // todo add comments - doc string

  IN_TRANSIT,

  OUT_FOR_DELIVERY,

  DELIVERED,

  RETURNED_TO_SENDER,

  ON_HOLD,

  LOST,

  DAMAGED
}

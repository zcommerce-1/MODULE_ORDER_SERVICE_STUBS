package com.crio.zcommerce.backend.order.dao.repository;

import com.crio.zcommerce.backend.order.dao.entity.PaymentEntity;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<PaymentEntity, Long> {

  Optional<PaymentEntity> findById(Long paymentId);
}

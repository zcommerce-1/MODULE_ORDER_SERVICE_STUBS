package com.crio.zcommerce.backend.order.common.model;

import jakarta.validation.constraints.NotNull;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Item extends BaseModel {

  @NotNull private String code;

  @NotNull private String name;

  @NotNull private String title;

  private List<String> imageUrls;

  private Double price;

  @NotNull private String shortDescription;

  private String longDescription;

  private Inventory inventory;
}

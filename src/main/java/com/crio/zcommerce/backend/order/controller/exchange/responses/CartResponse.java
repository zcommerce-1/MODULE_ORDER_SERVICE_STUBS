package com.crio.zcommerce.backend.order.controller.exchange.responses;

import com.crio.zcommerce.backend.order.model.BaseModel;
import com.crio.zcommerce.backend.order.model.ItemDetailsQuantity;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CartResponse extends BaseModel {

  @NotNull private String userId;

  private List<ItemDetailsQuantity> availableItem;

  private List<ItemDetailsQuantity> outOfStockItem;
}

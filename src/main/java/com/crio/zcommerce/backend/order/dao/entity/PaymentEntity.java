package com.crio.zcommerce.backend.order.dao.entity;

import com.crio.zcommerce.backend.order.model.enums.PaymentGateway;
import com.crio.zcommerce.backend.order.model.enums.PaymentStatus;
import com.crio.zcommerce.backend.order.model.enums.PaymentType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(
    name = "payment",
    indexes = {
      @Index(name = "IDX_orderId", columnList = "order_id"),
      @Index(name = "IDX_paymentType", columnList = "paymentType"),
      @Index(name = "IDX_paymentStatus", columnList = "paymentStatus"),
    })
public class PaymentEntity extends BaseEntity {
  @ManyToOne
  @JoinColumn(name = "order_id", nullable = false)
  @ToString.Exclude
  private OrderEntity order;

  private Long transactionId;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private PaymentType paymentType;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private PaymentStatus paymentStatus;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private PaymentGateway paymentGateway;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    PaymentEntity that = (PaymentEntity) o;
    return getId() != null && Objects.equals(getId(), that.getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}

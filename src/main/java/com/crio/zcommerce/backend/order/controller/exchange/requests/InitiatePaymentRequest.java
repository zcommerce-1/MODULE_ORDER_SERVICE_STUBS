package com.crio.zcommerce.backend.order.controller.exchange.requests;

import com.crio.zcommerce.backend.order.model.enums.PaymentGateway;
import com.crio.zcommerce.backend.order.model.enums.PaymentType;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InitiatePaymentRequest {

  /*
   *  UserId will be populated from the principle at controller layer.
   *  So @NotNull validation is not made
   */
  private String userId;

  @NotNull private Long orderId;

  @NotNull private PaymentType paymentType;

  @NotNull private PaymentGateway paymentGateway;
}

package com.crio.zcommerce.backend.order.common.exceptions;

/**
 * A custom exception class.
 * Cause: Successful payment already exists for order.
 * @param String message: The error message.
 */
public class PaymentAlreadyMadeException extends RuntimeException {
  public PaymentAlreadyMadeException(String message) {
    super(message);
  }
}

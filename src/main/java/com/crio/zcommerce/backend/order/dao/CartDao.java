package com.crio.zcommerce.backend.order.dao;

import com.crio.zcommerce.backend.order.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.order.model.Cart;
import java.util.Optional;

public interface CartDao {

  Optional<Cart> findByUserId(String userId);

  Cart createCart(Cart cart);

  Cart updateCart(Long cartId, Cart cart) throws EntityDoesNotExistException;

  void deleteCart(Long cartId);
}

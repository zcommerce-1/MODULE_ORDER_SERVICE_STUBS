package com.crio.zcommerce.backend.order.model;

import com.crio.zcommerce.backend.order.model.enums.PaymentGateway;
import com.crio.zcommerce.backend.order.model.enums.PaymentStatus;
import com.crio.zcommerce.backend.order.model.enums.PaymentType;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Payment extends BaseModel {

  @NotNull private Order order;

  @NotNull private Long transactionId;

  @NotNull private PaymentType paymentType;

  @NotNull private PaymentStatus paymentStatus;

  @NotNull private PaymentGateway paymentGateway;
}

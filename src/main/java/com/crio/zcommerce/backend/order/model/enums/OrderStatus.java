package com.crio.zcommerce.backend.order.model.enums;

public enum OrderStatus {
  // todo add comments - doc string
  INITIATED,

  IN_PROGRESS,

  FAILED,

  COMPLETED,

  CANCELLED
}

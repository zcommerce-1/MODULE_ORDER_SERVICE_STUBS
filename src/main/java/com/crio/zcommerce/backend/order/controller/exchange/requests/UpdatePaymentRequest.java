package com.crio.zcommerce.backend.order.controller.exchange.requests;

import com.crio.zcommerce.backend.order.model.enums.PaymentStatus;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UpdatePaymentRequest {

  @NotNull private Long transactionId;

  @NotNull private PaymentStatus paymentStatus;
}

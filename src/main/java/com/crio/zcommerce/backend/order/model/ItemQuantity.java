package com.crio.zcommerce.backend.order.model;

import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import lombok.*;

@Data
@Builder
@EqualsAndHashCode
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class ItemQuantity implements Serializable {

  @NotNull private Long itemId;

  @NotNull private int quantity;
}

package com.crio.zcommerce.backend.order.model;

import com.crio.zcommerce.backend.order.model.enums.TrackingStatus;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Tracking extends BaseModel {

  @NotNull private String url;

  @NotNull private TrackingStatus trackingStatus;
}

package com.crio.zcommerce.backend.order.service;

import com.crio.zcommerce.backend.order.common.exceptions.ApiException;
import com.crio.zcommerce.backend.order.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.order.controller.exchange.responses.CartResponse;
import com.crio.zcommerce.backend.order.model.Cart;
import com.crio.zcommerce.backend.order.model.ItemQuantity;
import java.io.IOException;

public interface CartService {

  /**
   * Get cart details for the given userId
   *
   * @param userId of type string
   * @return cart details if present
   * @throws EntityDoesNotExistException when cart details is not found against given userId
   */
  Cart getCartDetails(String userId) throws EntityDoesNotExistException;

  /**
   * Get cart details for the given userId
   *
   * @param userId of type string
   * @return cart details if present
   * @throws EntityDoesNotExistException when cart details is not found against given userId
   */
  CartResponse getUserCartDetails(String userId)
      throws EntityDoesNotExistException, IOException, ApiException;

  /**
   * Create cart
   *
   * @param cart of type Cart
   * @return cart details of the actual cart created
   */
  Cart createCart(Cart cart);

  /**
   * Update cart
   *
   * @param cartId of type long
   * @param cart of type Cart to update
   * @return updated cart details
   */
  Cart updateCart(Long cartId, Cart cart) throws EntityDoesNotExistException;

  /**
   * Add Item in cart
   *
   * @param userId of type String
   * @param itemQuantity of type ItemQuantity to add
   * @return updated cart details
   */
  Cart updateItemInCart(String userId, ItemQuantity itemQuantity)
      throws EntityDoesNotExistException, ApiException, IOException;

  /**
   * Delete cart details
   *
   * @param cartId of type Cart
   */
  void deleteCart(Long cartId);
}

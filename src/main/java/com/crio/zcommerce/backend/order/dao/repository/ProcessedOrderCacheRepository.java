package com.crio.zcommerce.backend.order.dao.repository;

import com.crio.zcommerce.backend.order.dao.entity.ProcessedOrderCache;
import org.springframework.data.repository.CrudRepository;


public interface ProcessedOrderCacheRepository extends CrudRepository<ProcessedOrderCache, String> {
}

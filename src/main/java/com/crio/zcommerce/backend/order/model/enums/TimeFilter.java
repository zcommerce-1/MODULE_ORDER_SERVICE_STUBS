package com.crio.zcommerce.backend.order.model.enums;

public enum TimeFilter {
  LAST_3_MONTH,

  LAST_6_MONTH,

  LAST_YEAR,
}

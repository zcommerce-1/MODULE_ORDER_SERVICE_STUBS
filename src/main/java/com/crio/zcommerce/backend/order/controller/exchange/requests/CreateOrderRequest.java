package com.crio.zcommerce.backend.order.controller.exchange.requests;

import com.crio.zcommerce.backend.order.model.ItemQuantity;
import com.crio.zcommerce.backend.order.model.enums.PaymentType;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateOrderRequest {

  /*
   *  UserId will be populated from the principle at controller layer.
   *  So @NotNull validation is not made
   */
  private String userId;

  @NotNull private List<ItemQuantity> orderItemsDetail;

  @NotNull private PaymentType paymentType;

  @NotNull private String shippingAddress;

  @NotNull private String billingAddress;
}

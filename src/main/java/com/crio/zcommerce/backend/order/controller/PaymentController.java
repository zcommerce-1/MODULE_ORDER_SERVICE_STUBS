package com.crio.zcommerce.backend.order.controller;

import com.crio.zcommerce.backend.order.common.api.ApiResponse;
import com.crio.zcommerce.backend.order.common.api.ApiResponseStatus;
import com.crio.zcommerce.backend.order.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.order.common.exceptions.ForbiddenException;
import com.crio.zcommerce.backend.order.common.exceptions.PaymentAlreadyMadeException;
import com.crio.zcommerce.backend.order.controller.exchange.requests.InitiatePaymentRequest;
import com.crio.zcommerce.backend.order.controller.exchange.requests.UpdatePaymentRequest;
import com.crio.zcommerce.backend.order.model.Payment;
import com.crio.zcommerce.backend.order.service.PaymentService;
import jakarta.validation.Valid;
import java.security.Principal;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.metrics.annotation.Timed;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Timed
@RestController
@RequiredArgsConstructor
@RequestMapping(PaymentController.PAYMENT_API_PREFIX)
public class PaymentController {
  public static final String PAYMENT_API_PREFIX = "/api/v1/payment";
  private static final String ID_ENDPOINT = "/{id}";
  private final PaymentService paymentService;

  /**
   * URL: /api/v1/payment/{id}
   * Description: Get payment details of the given id
   * Method: GET
   *
   * @return payment details
   */
  @GetMapping(ID_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_USER')")
  public ResponseEntity<ApiResponse<Payment>> getPaymentDetails(
      @PathVariable("id") Long paymentId, Principal principal) {
    ApiResponse<Payment> response = new ApiResponse<>();
    String userId = principal.getName();
    try {
      Payment payment = paymentService.getPaymentDetails(paymentId, userId);
      response.setData(payment);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      // todo pass the exception message directly
      response.setMessage(
          String.format("payment details not found for the given paymentId : %s", paymentId));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    } catch (ForbiddenException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(
          String.format(
              "Get payment failed as paymentId: %s do not belong to userId: %s",
              paymentId, userId));
      return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
    }
  }

  /**
   * URL: /api/v1/payment
   * Description: Initiate a payment request
   * Method: POST
   *
   * @param initiatePaymentRequest of type InitiatePaymentRequest
   * @return Initiated payment details
   */
  @PostMapping
  @PreAuthorize("hasAuthority('ROLE_USER')")
  public ResponseEntity<ApiResponse<Payment>> initiatePayment(
      @RequestBody @Valid InitiatePaymentRequest initiatePaymentRequest, Principal principal) {
    initiatePaymentRequest.setUserId(principal.getName());
    ApiResponse<Payment> response = new ApiResponse<>();

    try {
      // Creating cart for the user
      Payment payment = paymentService.initiatePayment(initiatePaymentRequest);
      response.setData(payment);
      response.setStatus(ApiResponseStatus.SUCCESS);
      response.setMessage("Payment process is initiated");
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    } catch (PaymentAlreadyMadeException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }
  }

  /**
   * URL: /api/v1/payment/{id}
   * Description: Update payment details
   * Method: PUT
   *
   * @param updatePaymentRequest of type UpdatePaymentRequest
   * @return updated payment details
   */
  @PatchMapping(ID_ENDPOINT)
  public ResponseEntity<ApiResponse<Payment>> updateOrderStatus(
      @PathVariable("id") Long paymentId,
      @RequestBody @Valid UpdatePaymentRequest updatePaymentRequest) {
    // todo Authenticate using API key.
    ApiResponse<Payment> response = new ApiResponse<>();
    try {
      Payment payment = paymentService.updatePayment(paymentId, updatePaymentRequest);
      response.setData(payment);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/payment/{id}
   * Description: Delete payment details of given payment id
   * Method: DELETE
   */
  // todo note: this should be an admin api
  @DeleteMapping(ID_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  public ResponseEntity<ApiResponse<String>> deleteOrder(@PathVariable("id") Long paymentId) {
    paymentService.deletePayment(paymentId);
    ApiResponse<String> response = new ApiResponse<>();
    response.setStatus(ApiResponseStatus.SUCCESS);
    response.setMessage("Payment details deleted");
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }
}

package com.crio.zcommerce.backend.order.model;

import com.crio.zcommerce.backend.order.common.model.Item;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class ItemDetailsQuantity {

  @NotNull private Item item;

  @NotNull private int quantity;
}

package com.crio.zcommerce.backend.order.config.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.security.oauth2.server.resource.InvalidBearerTokenException;
import org.springframework.security.oauth2.server.resource.web.BearerTokenAuthenticationEntryPoint;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.context.RequestAttributeSecurityContextRepository;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class CognitoTokenFilter extends OncePerRequestFilter {


  private final CognitoConfig config;

  private final AuthenticationEntryPoint authenticationEntryPoint = new BearerTokenAuthenticationEntryPoint();
  private final SecurityContextRepository securityContextRepository = new RequestAttributeSecurityContextRepository();
  private final SecurityContextHolderStrategy securityContextHolderStrategy = SecurityContextHolder
          .getContextHolderStrategy();

  public CognitoTokenFilter(CognitoConfig config) {
    this.config = config;
  }

  private RequestMatcher permitAllRoutesMatcher() {
    List<RequestMatcher> permitAllMatchers =
        Arrays.asList(
            new AntPathRequestMatcher("/swagger-ui/**"),
            new AntPathRequestMatcher("/v3/api-docs/**"),
            new AntPathRequestMatcher("/public/**"));
    return new OrRequestMatcher(permitAllMatchers);
  }

  private boolean isPermitAllEndpoint(HttpServletRequest request) {
    return permitAllRoutesMatcher().matches(request);
  }

  @Override
  protected void doFilterInternal(
      @NotNull HttpServletRequest request,
      @NotNull HttpServletResponse response,
      @NotNull FilterChain filterChain)
      throws ServletException, IOException {

    // If it's a permit-all endpoint, proceed without checking token
    if (isPermitAllEndpoint(request)) {
      filterChain.doFilter(request, response);
      return;
    }

    // Get token
    String token = request.getHeader("Authorization");
    if (token != null && token.startsWith("Bearer ")) {
      token = token.substring(7);
    }

    if (token != null ) {
      // Check if admin token
      if (Objects.equals(token, ADMIN_TOKEN)) {
        String userId = "admin-user";

        setSecurityContext(getClaims(
                userId,
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_ADMIN"))),
              request, response);

        filterChain.doFilter(request, response);
        return;
      }

      // Check if Impersonated token
      if (token.contains(IMPERSONATE_USER_TOKEN_CONTAINS)) {
        String userId = token.split(IMPERSONATE_USER_TOKEN_CONTAINS)[1].toLowerCase(Locale.ENGLISH);
        // todo: Get user from user service to check if valid user

        setSecurityContext(getClaims(
                userId,
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"))),
              request, response);

        filterChain.doFilter(request, response);
        return;
      }

      try {
        // If Cognito Token
        Jws<Claims> claims = SecurityUtil.validateToken(token, config.getIssuerUri());

        // Assign granted Authorities (cognito:group as ROLES)
        List<GrantedAuthority> grantedAuthorities =
                Optional.ofNullable((List<String>) claims.getBody().get("cognito:groups"))
                        .orElse(Collections.emptyList())
                        .stream()
                        .map(group -> new SimpleGrantedAuthority("ROLE_" + group))
                        .collect(Collectors.toList());

        setSecurityContext(getClaims(
                claims.getBody().get("username"),
                grantedAuthorities),
              request, response);

      } catch (Exception ex) {
        logger.error(ex);
        this.authenticationEntryPoint.commence(request, response, new InvalidBearerTokenException("Invalid Token"));
        return;
      }
    }

    filterChain.doFilter(request, response);
  }

  @NotNull
  private static UsernamePasswordAuthenticationToken getClaims(Object userId,
                                Collection<? extends org.springframework.security.core.GrantedAuthority> authorities) {
    return new UsernamePasswordAuthenticationToken(
            userId,
            null,
            authorities);
  }

  private void setSecurityContext(UsernamePasswordAuthenticationToken claims,
                                  @NotNull HttpServletRequest request,
                                  @NotNull HttpServletResponse response) {
    // Set Authentication in SecurityContextHolder, username as Principal.
    Authentication auth = claims;

    SecurityContext context = this.securityContextHolderStrategy.createEmptyContext();
    context.setAuthentication(auth);
    this.securityContextHolderStrategy.setContext(context);
    this.securityContextRepository.saveContext(context, request, response);
  }
}

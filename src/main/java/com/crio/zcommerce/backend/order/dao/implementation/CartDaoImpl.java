package com.crio.zcommerce.backend.order.dao.implementation;

import com.crio.zcommerce.backend.order.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.order.dao.CartDao;
import com.crio.zcommerce.backend.order.dao.entity.CartEntity;
import com.crio.zcommerce.backend.order.dao.repository.CartRepository;
import com.crio.zcommerce.backend.order.model.Cart;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class CartDaoImpl implements CartDao {

  private final CartRepository cartRepository;
  private final ModelMapper modelMapper;

  @Override
  public Optional<Cart> findByUserId(String userId) {
    return cartRepository.findByUserId(userId).map(cart -> modelMapper.map(cart, Cart.class));
  }

  @Override
  public Cart createCart(Cart cart) {
    CartEntity cartEntity = modelMapper.map(cart, CartEntity.class);
    // inserting cart entity into cart table
    return modelMapper.map(cartRepository.save(cartEntity), Cart.class);
  }

  @Override
  public Cart updateCart(Long cartId, Cart cart) throws EntityDoesNotExistException {
    String logPrefix = "event: updateCart, message: ";
    // fetching the existing cart entity for the cartId
    Optional<CartEntity> existingCartEntityOptional = cartRepository.findById(cartId);
    if (!existingCartEntityOptional.isPresent()) {
      log.info(logPrefix + "cart details not found for the cartId : {}", cartId);
      throw new EntityDoesNotExistException(
          String.format(
              "%s update failed as cart details not found for the cartId : %s", logPrefix, cartId));
    }
    CartEntity cartEntity = existingCartEntityOptional.get();
    cartEntity.setItemQuantityList(cart.getItemQuantityList());
    CartEntity updateCartEntity = cartRepository.save(cartEntity);
    return modelMapper.map(updateCartEntity, Cart.class);
  }

  @Override
  public void deleteCart(Long cartId) {
    cartRepository.deleteById(cartId);
  }
}

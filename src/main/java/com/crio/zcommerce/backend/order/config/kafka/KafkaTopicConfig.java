package com.crio.zcommerce.backend.order.config.kafka;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;

@EnableKafka
@Configuration
@ConfigurationProperties(prefix = "kafka")
@Data
public class KafkaTopicConfig {

  @Value("${kafka.topic.order.name}")
  private String orderEventTopic;

  @Value("${kafka.topic.webhook-trigger.name}")
  private String webhookTriggerTopic;

  @Value("${kafka.topic.order.consumer.notification}")
  private String orderNotificationConsumerGroup;

  @Value("${kafka.topic.order.consumer.webhook}")
  private String orderWebhookConsumerGroup;

  @Value("${kafka.bootstrapAddress}")
  private String bootstrapAddress;
}

package com.crio.zcommerce.backend.order.dao.implementation;

import com.crio.zcommerce.backend.order.dao.ProcessedOrdersDao;
import com.crio.zcommerce.backend.order.dao.entity.ProcessedOrderCache;
import com.crio.zcommerce.backend.order.dao.repository.ProcessedOrderCacheRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class ProcessedOrdersDaoImpl implements ProcessedOrdersDao {

    private final ProcessedOrderCacheRepository repository;

    @Override
    public void create(String idempotencyToken) {
        ProcessedOrderCache processedOrderCache = new ProcessedOrderCache(idempotencyToken);
        processedOrderCache.setExpiration(3600L);

        repository.save(processedOrderCache);
    }

    @Override
    public boolean exists(String idempotencyToken) {
        return repository.existsById(idempotencyToken);
    }
}

package com.crio.zcommerce.backend.order.service;

import com.crio.zcommerce.backend.order.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.order.controller.exchange.requests.InitiatePaymentRequest;
import com.crio.zcommerce.backend.order.controller.exchange.requests.UpdatePaymentRequest;
import com.crio.zcommerce.backend.order.model.Payment;

public interface PaymentService {

  /**
   * Get the payment details for the given paymentId
   * @param paymentId of type long
   * @return Payment details
   * @throws EntityDoesNotExistException if payment details not found for the paymentId
   */
  Payment getPaymentDetails(Long paymentId, String userid) throws EntityDoesNotExistException;

  /**
   * Initiate the payment process for the user based on the payment type and gateway
   * @param initiatePaymentRequest consists of userId, orderId, paymentType and paymentGateway
   * @return payment details
   */
  Payment initiatePayment(InitiatePaymentRequest initiatePaymentRequest)
      throws EntityDoesNotExistException;

  /**
   * Update the payment status and transaction id after the payment process
   * @param  updatePaymentRequest paymentStatus and transactionId
   * @return payment details
   */
  Payment updatePayment(Long paymentId, UpdatePaymentRequest updatePaymentRequest)
      throws EntityDoesNotExistException;

  /**
   * Delete the payment details for the given paymentId
   */
  void deletePayment(Long paymentId);
}

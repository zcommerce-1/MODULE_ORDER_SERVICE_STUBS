package com.crio.zcommerce.backend.order.clients;

import com.crio.zcommerce.backend.order.common.exceptions.ApiException;
import com.crio.zcommerce.backend.order.common.exceptions.NetworkException;
import com.crio.zcommerce.backend.order.config.AppConfig;
import com.crio.zcommerce.backend.order.model.ItemQuantity;
import com.crio.zcommerce.backend.order.model.Order;
import java.io.IOException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import okhttp3.Headers;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Log4j2
public class NotificationClient {

  @Value("${com.crio.zcommerce.backend.order.clients.notificationClient.url}")
  private String notificationUrl;

  private static final String NOTIFICATION_URL = "api/v1/event";

  private Headers headers;

  private final AppConfig appConfig;
  private final NetworkClient networkClient;

  public void sendOrderNotification(Order order) throws IOException, ApiException {
    String payload = createOrderNotificationRequestBody(order);

    String url = notificationUrl + NOTIFICATION_URL;

    try {
      Response response = networkClient.post(url, new Headers.Builder().build(), payload);
      String responseBody = response.body().string();
      if (response.isSuccessful()) {
        log.info(
                "event: sendOrderNotification(), "
                        + "msg: Order notification sent, orderId : {}, response: {}",
                order.getId(),
                responseBody);
      } else {
        log.error(
                "event: sendOrderNotification(), "
                        + "msg: Order notification failed, orderId : {}, response: {}",
                order.getId(),
                responseBody);
        throw new ApiException(
                String.format(
                        "event: sendOrderNotification(), "
                                + "msg: Order notification failed, orderId : %s, response: %s",
                        order.getId(), responseBody));
      }
    } catch (IOException io) {
      throw new NetworkException(
              String.format(
                      "Order notification failed, orderId : %s, response: %s",
                      order.getId(), io.getMessage()));
    }
  }

  /*
  {
    "name": "string",
      "timestamp": "2023-06-14T10:21:24.657Z",
      "recipient": {
    "full_name": "string",
        "email": "user@example.com",
        "phone": "string"
  },
    "payload": {
    "order_id": "string",
        "items": [
    {
      "id": 0,
        "name": "string",
        "quantity": 0,
        "price": 0
    }
    ],
    "subtotal": 0,
    "discount": 0,
    "taxes": 0,
    "total": 0
  }
  }
  */
  public String createOrderNotificationRequestBody(Order order) {
    JSONObject recipient = new JSONObject();
    recipient.put("full_name", order.getUserId());
    recipient.put("email", "abc@gmail.com");
    recipient.put("phone", "9992342239");

    JSONArray items = new JSONArray();
    for (ItemQuantity itemQuantity : order.getItemQuantityList()) {
      JSONObject item = new JSONObject();
      item.put("name", "item name");
      item.put("id", itemQuantity.getItemId());
      item.put("quantity", itemQuantity.getQuantity());
      item.put("price", 2.0);
      items.put(item);
    }

    JSONObject payload = new JSONObject();
    payload.put("order_id", order.getId());
    payload.put("items", items);
    payload.put("total", order.getAmount());
    payload.put("discount", order.getAmount());
    payload.put("taxes", order.getAmount());
    payload.put("subtotal", order.getAmount());

    JSONObject notificationData = new JSONObject();
    notificationData.put("name", order.getUserId());
    notificationData.put("recipient", recipient);
    notificationData.put("payload", payload);

    return JSONObject.valueToString(notificationData);
  }
}

package com.crio.zcommerce.backend.order.clients;

import com.crio.zcommerce.backend.order.common.exceptions.ApiException;
import com.crio.zcommerce.backend.order.common.exceptions.NetworkException;
import com.crio.zcommerce.backend.order.common.model.Item;
import com.crio.zcommerce.backend.order.config.AppConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.io.IOException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import okhttp3.Headers;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Log4j2
public class ProductClient {

  private static final String PRODUCT_API = "api/v1/item/";

  private Headers headers;

  private final AppConfig appConfig;

  private final NetworkClient networkClient;

  @Value("${com.crio.zcommerce.backend.order.clients.productClient.url}")
  private String itemServiceUrl;

  public Item getProductDetails(Long itemId) throws IOException, ApiException {

    String getItemDetailsUrl = itemServiceUrl + PRODUCT_API + itemId.toString();

    try {
      Response response = networkClient.get(getItemDetailsUrl, new Headers.Builder().build());
      String responseBody = response.body().string();
      if (response.isSuccessful()) {
        JsonNode jsonNode = getObjectMapper().readValue(responseBody, JsonNode.class);
        Item item = getObjectMapper().readValue(jsonNode.get("data").toString(), Item.class);
        // todo check why quantityInStock is not mapping
        // item.getInventory().setQuantityInStock(
        // jsonNode.get("data").get("inventory").get("quantityInStock").asInt());
        return item;
      } else {
        log.error(
            "event: getProductDetails(), "
                + "msg: getProductDetails failed, itemId : {}, response: {}",
            itemId,
            responseBody);
        throw new ApiException(
            String.format(
                "event: getProductDetails(), "
                    + "msg: getProductDetails failed, itemId : %s, response: %s",
                itemId, responseBody));
      }
    } catch (IOException io) {
      throw new NetworkException(
          String.format(
              "event: getProductDetails failed, itemId : %s" + "error : %s ",
              itemId, io.getMessage()));
    }
  }

  // todo move to util
  public ObjectMapper getObjectMapper() {
    ObjectMapper mapper = new ObjectMapper();
    mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    mapper.registerModule(new JavaTimeModule());
    return mapper;
  }
}

package com.crio.zcommerce.backend.order.dao.entity;

import com.crio.zcommerce.backend.order.model.enums.TrackingStatus;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import java.util.Objects;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "tracking")
public class TrackingEntity extends BaseEntity {

  @OneToOne(mappedBy = "tracking")
  private OrderEntity order;

  @Column(nullable = false)
  private String url;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private TrackingStatus trackingStatus;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    TrackingEntity that = (TrackingEntity) o;
    return getId() != null && Objects.equals(getId(), that.getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}

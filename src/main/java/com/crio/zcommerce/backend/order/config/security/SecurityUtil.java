package com.crio.zcommerce.backend.order.config.security;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.UrlJwkProvider;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.util.Base64;
import java.util.Map;
import lombok.NonNull;

public class SecurityUtil {

  public static Jws<Claims> validateToken(@NonNull String token, @NonNull String jwtIssuerUri)
      throws MalformedURLException, JsonProcessingException {

    try {
      // Get JwkProvider to get Public key to verify the token's signature
      JwkProvider jwkProvider =
          new UrlJwkProvider(new URL(jwtIssuerUri + "/.well-known/jwks.json"));

      // Get kid from token's header
      String header = token.split("\\.")[0];
      String decodedHeader = new String(Base64.getDecoder().decode(header), StandardCharsets.UTF_8);
      ObjectMapper mapper = new ObjectMapper();
      Map<String, Object> headerMap = mapper.readValue(decodedHeader, Map.class);
      String kid = (String) headerMap.get("kid");

      // Get Public key from kid
      Jwk jwk = jwkProvider.get(kid);
      PublicKey publicKey = jwk.getPublicKey();

      // Decode, Validate and Verify token
      Jws<Claims> claims = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token);

      // Ensure the token is from the expected issuer
      if (!claims.getBody().getIssuer().equals(jwtIssuerUri)) {
        throw new JwtException("Unexpected Issuer");
      }
      return claims;
    } catch (JwkException | JwtException ex) {
      // Invalid JWT token
      throw new SecurityException("Failed to validate token", ex);
    }
  }
}

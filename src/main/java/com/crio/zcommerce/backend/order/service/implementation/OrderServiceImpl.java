package com.crio.zcommerce.backend.order.service.implementation;

import com.crio.zcommerce.backend.order.clients.ProductClient;
import com.crio.zcommerce.backend.order.clients.UserClient;
import com.crio.zcommerce.backend.order.common.exceptions.*;
import com.crio.zcommerce.backend.order.common.model.Item;
import com.crio.zcommerce.backend.order.config.kafka.KafkaTopicConfig;
import com.crio.zcommerce.backend.order.controller.exchange.requests.CreateOrderRequest;
import com.crio.zcommerce.backend.order.controller.exchange.requests.UpdateOrderStatusRequest;
import com.crio.zcommerce.backend.order.dao.CartDao;
import com.crio.zcommerce.backend.order.dao.OrderDao;
import com.crio.zcommerce.backend.order.dao.ProcessedOrdersDao;
import com.crio.zcommerce.backend.order.model.*;
import com.crio.zcommerce.backend.order.model.enums.OrderStatus;
import com.crio.zcommerce.backend.order.model.enums.PaymentType;
import com.crio.zcommerce.backend.order.model.enums.TimeFilter;
import com.crio.zcommerce.backend.order.service.OrderService;
import com.crio.zcommerce.backend.order.utils.kafka.KafkaUtil;
import com.crio.zcommerce.backend.order.utils.kafka.model.ConsumerEvent;
import com.crio.zcommerce.backend.order.utils.kafka.model.OrderEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Log4j2
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

  private final OrderDao orderDao;

  private final CartDao cartDao;

  private final KafkaTopicConfig kafkaTopicConfig;

  private final KafkaUtil kafkaUtil;

  private final ProductClient productClient;

  private final UserClient userClient;

  private final ModelMapper modelMapper;

  private final ProcessedOrdersDao processedOrdersDao;

  @Override
  public Order getOrderDetails(String userId, Long orderId) throws EntityDoesNotExistException {
    String logPrefix = "event: getOrderDetails, message: ";
    Optional<Order> orderOptional = orderDao.findByUserIdAndOrderId(userId, orderId);

    if (orderOptional.isPresent()) {
      return orderOptional.get();
    } else {
      // throwing entity not found exception as the order is not found
      log.info(
          logPrefix + "order details not found for the given userId : {} and orderId : {}",
          userId,
          orderId);
      throw new EntityDoesNotExistException(
          String.format(
              "%s order details not found for the given userId : %s and orderId : %s",
              logPrefix, userId, orderId));
    }
  }

  @Override
  public OrderItems getPopulatedOrderDetails(String userId, Long orderId)
      throws EntityDoesNotExistException, IOException, ApiException {
    String logPrefix = "event: getPopulatedOrderDetails, message: ";
    Order order = getOrderDetails(userId, orderId);
    return populateOrder(order);
  }

  private OrderItems populateOrder(Order order) throws IOException, ApiException {
    OrderItems orderItems = modelMapper.map(order, OrderItems.class);
    orderItems.setItemDetailsQuantity(new ArrayList<>());

    for (ItemQuantity itemQuantity : order.getItemQuantityList()) {
      Item item = productClient.getProductDetails(itemQuantity.getItemId());
      ItemDetailsQuantity itemDetailsQuantity =
          ItemDetailsQuantity.builder().item(item).quantity(itemQuantity.getQuantity()).build();
      orderItems.getItemDetailsQuantity().add(itemDetailsQuantity);
    }

    return orderItems;
  }

  @Override
  public Page<OrderItems> getAllOrders(String userId, Pageable pageable) {
    String logPrefix = "event: getAllOrders, message: ";
    log.info(logPrefix + "fetching all order for the userId : {}", userId);
    return orderDao
        .findAllByUserIdSortedByDateDesc(userId, pageable)
        .map(
            order -> {
              try {
                return populateOrder(order);
              } catch (IOException | ApiException e) {
                throw new RuntimeException(e);
              }
            });
  }

  @Override
  public Page<OrderItems> getOrdersByTimeFilter(
      String userId, TimeFilter timeFilter, Pageable pageable) {
    String logPrefix = "event: getOrdersByTimePeriod, message: ";
    log.info(logPrefix + "fetching all order for the userId for given time period: {}", userId);
    LocalDate startDate = getStartDateBasedOnFilter(timeFilter);
    ZonedDateTime zonedStartDateTime = startDate.atStartOfDay(ZoneId.systemDefault());
    Page<OrderItems> allOrders =
        orderDao
            .findAllByUserIdAndCreatedAtGreaterThanEqual(userId, zonedStartDateTime, pageable)
            .map(
                order -> {
                  try {
                    return populateOrder(order);
                  } catch (IOException | ApiException e) {
                    throw new RuntimeException(e);
                  }
                });

    return allOrders;
  }

  private LocalDate getStartDateBasedOnFilter(TimeFilter timeFilter) {
    LocalDate now = LocalDate.now();
    switch (timeFilter) {
      case LAST_3_MONTH:
        return now.minusMonths(3);
      case LAST_6_MONTH:
        return now.minusMonths(6);
      case LAST_YEAR:
        return now.minusYears(1);
      default:
        throw new IllegalArgumentException("Unsupported TimeFilter");
    }
  }

  @Override
  public Order createOrder(CreateOrderRequest createOrderRequest)
      throws InvalidUserException, IOException, ApiException, OutOfStockException {
    String logPrefix = "event: createOrder, message: ";
    // check if user is eligible to create an order if not throw invalid user exception
    if (!isUserValid(createOrderRequest.getUserId())) {
      String errorMessage =
          String.format(
              "user is not a valid user so skipping the order creation, userId: %s",
              createOrderRequest.getUserId());
      log.info(logPrefix + errorMessage);
      throw new InvalidUserException(errorMessage);
    }

    // check if the items in the order are valid and are in stock
    List<ItemQuantity> orderItemDetails = createOrderRequest.getOrderItemsDetail();
    if (!isValidProductsAndInStock(orderItemDetails)) {
      String errorMessage = "Some of the products are not valid or not in stock";
      log.info(logPrefix + errorMessage);
      throw new OutOfStockException(errorMessage);
    }

    // calculate the total amount of the order
    Double orderAmount = calculateOrderAmount(orderItemDetails);

    // determining order status based on the payment type
    // once the payment is completed, update the order details with payment and tracking info
    OrderStatus orderStatus = OrderStatus.INITIATED;
    if (createOrderRequest.getPaymentType().equals(PaymentType.CASH_ON_DELIVERY)) {
      orderStatus = OrderStatus.IN_PROGRESS;
    }

    // create an entry to order table with order in_progress status
    Order newOrder =
        Order.builder()
            .userId(createOrderRequest.getUserId())
            .itemQuantityList(orderItemDetails)
            .status(orderStatus)
            .amount(orderAmount)
            .shippingAddress(createOrderRequest.getShippingAddress())
            .billingAddress(createOrderRequest.getBillingAddress())
            .build();

    Order order = orderDao.createOrder(newOrder);
    log.info(
        logPrefix + "new order has been created for userId : {} , orderId : {}",
        createOrderRequest.getUserId(),
        order.getId());

    // Empty Cart after order creation
    Optional<Cart> cart = cartDao.findByUserId(order.getUserId());
    if (cart.isPresent()) {
      cartDao.deleteCart(cart.get().getId());
    }

    // send order creation event to the order topic
    sendOrderCreationEvent(order);

    return order;
  }

  @Override
  @Transactional
  public Order createOrderIdempotent(CreateOrderRequest createOrderRequest, String idempotencyHeader)
          throws IOException, InvalidUserException, ApiException, DuplicateRequestException {
    if (processedOrdersDao.exists(idempotencyHeader)) {
      throw new DuplicateRequestException("Duplicate Order. An identical order already exists.");
    }
    Order order = createOrder(createOrderRequest);
    processedOrdersDao.create(idempotencyHeader);
    return order;
  }

  private void sendOrderCreationEvent(Order order) {
    String logPrefix = "event: sendOrderCreationEvent, message: ";

    ConsumerEvent consumerEvent =
        OrderEvent.builder()
            .userId(order.getUserId())
            .order(order)
            .build();
    kafkaUtil.sendEvent(consumerEvent, kafkaTopicConfig.getOrderEventTopic());
    log.info(
        logPrefix + "sent event to order topic userId : {} , orderId : {}",
        order.getUserId(),
        order.getId());
  }

  private Double calculateOrderAmount(List<ItemQuantity> orderItemDetails)
      throws IOException, ApiException {
    Double totalAmount = 0.0;
    for (ItemQuantity itemQuantity : orderItemDetails) {
      Long itemId = itemQuantity.getItemId();
      int quantity = itemQuantity.getQuantity();

      // Perform the necessary logic to calculate the amount for each item
      // and add it to the total amount
      Double itemAmount = calculateItemAmount(itemId, quantity);
      totalAmount += itemAmount;
    }
    return totalAmount;
  }

  private Double calculateItemAmount(Long itemId, int quantity) throws IOException, ApiException {
    // Implement the logic to calculate the amount for a specific item
    // based on its identifier (itemId) and the quantity
    // You can fetch the item price from the product service
    // For demonstration purposes, let's assume the price for each item is 10

    Item item = productClient.getProductDetails(itemId);
    Double itemPrice = item.getPrice();
    return itemPrice * quantity;
  }

  private boolean isValidProductsAndInStock(List<ItemQuantity> orderItemDetails) {
    // calling the product service to get the inventory details of the item
    for (ItemQuantity orderItemQuantity : orderItemDetails) {
      try {
        Item item = productClient.getProductDetails(orderItemQuantity.getItemId());


        if (orderItemQuantity.getQuantity() <= item.getInventory().getQuantityInStock()) {
          return true;
        }
        // todo log
        return false;
      } catch (Exception e) {
        return false;
      }
    }
    return false;
  }

  private boolean isUserValid(String userId) {
    // todo user service call to validate
    try {
      return true;
    } catch (Exception e) {
    }
    return false;
  }

  @Override
  public Order updateOrderStatus(Long orderId, String userId, UpdateOrderStatusRequest request)
      throws EntityDoesNotExistException, ForbiddenException {
    String logPrefix = "event : updateOrderStatus, message: ";
    log.info(logPrefix + "Checking if orderId: {} belongs to user: {}", orderId, userId);
    orderDao
        .findByUserIdAndOrderId(userId, orderId)
        .orElseThrow(
            () ->
                new ForbiddenException(
                    String.format(
                        "%s order: %s do not belong to userId : %s ", logPrefix, orderId, userId)));

    log.info(logPrefix + "order is being updated for orderId : {}", orderId);
    // todo check if any validation required here - state management validation
    return orderDao.updateOrderStatus(orderId, request);
  }

  @Override
  public void deleteOrder(Long orderId) {
    String logPrefix = "event : updateOrderStatus, message: ";
    log.info(logPrefix + "order is being deleted for orderId : {}", orderId);
    orderDao.deleteOrder(orderId);
  }
}

package com.crio.zcommerce.backend.order.controller;

import com.crio.zcommerce.backend.order.common.exceptions.*;
import com.crio.zcommerce.backend.order.model.OrderItems;
import com.crio.zcommerce.backend.order.model.enums.TimeFilter;
import com.crio.zcommerce.backend.order.service.OrderService;
import java.security.Principal;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.metrics.annotation.Timed;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Timed
@RestController
@RequiredArgsConstructor
@RequestMapping(OrderControllerV2.ORDER_API_PREFIX)
public class OrderControllerV2 {

  private final OrderService orderService;

  public static final String ORDER_API_PREFIX = "/api/v2/order";

  public static final String ALL_ENDPOINT = "/all";

  /**
   * URL: /api/v2/order/all
   * Description: Get all orders of the user.
   * Method: GET
   *
   * @return Page of order
   */
  @GetMapping(ALL_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_USER')")
  public ResponseEntity<Page<OrderItems>> getAllOrders(
      @RequestParam(value = "timeFilter", required = true) TimeFilter timeFilter,
      @SortDefault(sort = "id") @PageableDefault(size = 20) Pageable pageable,
      Principal principal) {

    String userId = principal.getName();
    return ResponseEntity.status(HttpStatus.OK)
        .body(orderService.getOrdersByTimeFilter(userId, timeFilter, pageable));
  }
}

package com.crio.zcommerce.backend.order.dao.repository;

import com.crio.zcommerce.backend.order.dao.entity.OrderEntity;
import java.time.ZonedDateTime;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<OrderEntity, Long> {

  Optional<OrderEntity> findByIdAndUserId(Long orderId, String userId);

  Page<OrderEntity> findAllByUserIdOrderByCreatedAtDesc(String userId, Pageable pageable);

  Page<OrderEntity> findAllByUserIdAndCreatedAtGreaterThanEqual(
      String userId, ZonedDateTime startDateTime, Pageable pageable);
}

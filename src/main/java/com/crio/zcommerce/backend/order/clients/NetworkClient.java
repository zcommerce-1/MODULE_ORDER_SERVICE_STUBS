package com.crio.zcommerce.backend.order.clients;

import jakarta.annotation.PostConstruct;
import java.io.IOException;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import okhttp3.*;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Log4j2
@SuppressWarnings({
  "PMD.DataflowAnomalyAnalysis",
  "PMD.BeanMembersShouldSerialize",
  "PMD.AvoidDuplicateLiterals",
  "PMD.UseLocaleWithCaseConversions"
})
public class NetworkClient {
  private OkHttpClient client;

  private static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";
  private static final String CONTENT_TYPE_TEXT_PLAIN = "text/plain";

  @PostConstruct
  void init() {
    client = new OkHttpClient.Builder().build();
  }

  public Response post(String url, Headers headers, String payload) throws IOException {
    HttpUrl httpUrl = HttpUrl.parse(url);
    MediaType mediaType = MediaType.parse(CONTENT_TYPE_APPLICATION_JSON);
    RequestBody requestBody = RequestBody.create(mediaType, payload);

    Request request =
        new Request.Builder().url(url).method("POST", requestBody).headers(headers).build();

    return client.newCall(request).execute();
  }

  public Response get(String url, Headers headers) throws IOException {
    HttpUrl httpUrl = HttpUrl.parse(url);

    Request request =
        new Request.Builder().url(httpUrl).method("GET", null).headers(headers).build();

    return client.newCall(request).execute();
  }

  public Response put(String url, Headers headers, String payload) {
    HttpUrl httpUrl = HttpUrl.parse(url);
    MediaType mediaType = MediaType.parse(CONTENT_TYPE_APPLICATION_JSON);
    RequestBody requestBody = RequestBody.create(mediaType, payload);

    Request request =
        new Request.Builder().url(httpUrl).method("PUT", requestBody).headers(headers).build();

    Response response = null;
    try {
      response = client.newCall(request).execute();
      return response;
    } catch (IOException e) {
      return null;
    }
  }

  public Response delete(String url, Headers headers, String payload) throws IOException {
    HttpUrl httpUrl = HttpUrl.parse(url);
    MediaType mediaType = MediaType.parse(CONTENT_TYPE_APPLICATION_JSON);
    RequestBody requestBody = null;
    if (!Objects.isNull(payload)) {
      requestBody = RequestBody.create(mediaType, payload);
    }

    Request request =
        new Request.Builder().url(httpUrl).method("DELETE", requestBody).headers(headers).build();

    return client.newCall(request).execute();
  }
}

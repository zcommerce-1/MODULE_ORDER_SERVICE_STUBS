package com.crio.zcommerce.backend.order.dao;

import com.crio.zcommerce.backend.order.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.order.controller.exchange.requests.UpdatePaymentRequest;
import com.crio.zcommerce.backend.order.model.Payment;
import java.util.Optional;

public interface PaymentDao {

  Payment createPayment(Payment payment);

  Optional<Payment> findByPaymentId(Long paymentId);

  Payment updatePayment(Long paymentId, UpdatePaymentRequest request)
      throws EntityDoesNotExistException;

  void deletePayment(Long paymentId);
}

package com.crio.zcommerce.backend.order.common.api;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
 * Represents an API response containing status, message, and data.
 * The ApiResponse class is used to standardize the response format for API endpoints.
 * @param <T> The type of data included in the response.
 */
@Data
public class ApiResponse<T> {

  /**
   * API response of type success or failure filled based on the actual functionality status
   */
  @NotNull private ApiResponseStatus status;

  /**
   * Message of type string can contain additional information that needs to be sent
   */
  private String message;

  /**
   * This field stores the data associated with an object of type T.
   * It can hold various types depending on the usage context.
   */
  private T data;
}

package com.crio.zcommerce.backend.order.kafka.consumer;

import com.crio.zcommerce.backend.order.clients.NotificationClient;
import com.crio.zcommerce.backend.order.common.exceptions.NetworkException;
import com.crio.zcommerce.backend.order.utils.kafka.model.OrderEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class OrderNotificationConsumer {
    private final ObjectMapper objectMapper;
    private final NotificationClient notificationClient;

    private final String LISTENER_LOG_PREFIX = "event: OrderNotificationConsumer.listen, message: ";

    @KafkaListener(topics = "#{kafkaTopicConfig.orderEventTopic}",
            groupId = "#{kafkaTopicConfig.orderNotificationConsumerGroup}",
            containerFactory = "kafkaListenerContainerFactory")
    public void listen(String message) throws NetworkException {
        try {
            log.info(LISTENER_LOG_PREFIX + "message consumed from Kafka: {}", message);
            OrderEvent event = objectMapper.readValue(message, OrderEvent.class);

            // send order notification
            notificationClient.sendOrderNotification(event.getOrder());
        } catch (JsonProcessingException ex) {
            log.error(LISTENER_LOG_PREFIX + "JsonProcessingException thrown. Consumed message: {}",
                    message);
        } catch (NetworkException ex) {
            log.error(LISTENER_LOG_PREFIX + "NetworkException thrown.");
            throw ex; // Re-throw the exception to trigger retry
        } catch (Exception ex) {
            log.error(LISTENER_LOG_PREFIX + "Exception thrown. msg={}", ex.getMessage());
        }
    }
}

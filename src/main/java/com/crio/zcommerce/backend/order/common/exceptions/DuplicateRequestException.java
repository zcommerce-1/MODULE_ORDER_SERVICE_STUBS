package com.crio.zcommerce.backend.order.common.exceptions;

public class DuplicateRequestException extends Exception {

    public DuplicateRequestException() {
        super("The request has already been processed or a similar request is in progress.");
    }

    public DuplicateRequestException(String message) {
        super(message);
    }
}

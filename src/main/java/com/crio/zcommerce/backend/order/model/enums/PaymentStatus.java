package com.crio.zcommerce.backend.order.model.enums;

public enum PaymentStatus {
  // todo add comments - doc string
  INITIATED,

  IN_PROGRESS,

  FAILED,

  COMPLETED,

  REFUND_INITIATED,

  REFUND_COMPLETED
}

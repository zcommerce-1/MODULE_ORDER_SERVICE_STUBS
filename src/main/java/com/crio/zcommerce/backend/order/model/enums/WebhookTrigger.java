package com.crio.zcommerce.backend.order.model.enums;

public enum WebhookTrigger {
    NEW_ORDER,
    REMOVE_CART_ITEM,
    ADD_CART_ITEM
}

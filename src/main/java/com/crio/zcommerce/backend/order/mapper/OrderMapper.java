package com.crio.zcommerce.backend.order.mapper;

import com.crio.zcommerce.backend.order.dao.entity.OrderEntity;
import com.crio.zcommerce.backend.order.model.Order;
import org.mapstruct.*;

@Mapper(
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    componentModel = "spring",
    uses = PaymentMapper.class)
public interface OrderMapper {

  Order toDto(OrderEntity orderEntity);

  OrderEntity toEntity(Order order);

  @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
  OrderEntity updateEntity(Order order, @MappingTarget OrderEntity orderEntity);
}

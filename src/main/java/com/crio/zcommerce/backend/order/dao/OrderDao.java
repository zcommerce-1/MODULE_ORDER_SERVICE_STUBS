package com.crio.zcommerce.backend.order.dao;

import com.crio.zcommerce.backend.order.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.order.controller.exchange.requests.UpdateOrderStatusRequest;
import com.crio.zcommerce.backend.order.model.Order;
import java.time.ZonedDateTime;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OrderDao {

  Optional<Order> findByUserIdAndOrderId(String userId, Long orderId);

  Page<Order> findAllByUserIdSortedByDateDesc(String userId, Pageable pageable);

  Page<Order> findAllByUserIdAndCreatedAtGreaterThanEqual(
      String userId, ZonedDateTime startDateTime, Pageable pageable);

  Order createOrder(Order order);

  Order updateOrderStatus(Long orderId, UpdateOrderStatusRequest request)
      throws EntityDoesNotExistException;

  void deleteOrder(Long orderId);
}

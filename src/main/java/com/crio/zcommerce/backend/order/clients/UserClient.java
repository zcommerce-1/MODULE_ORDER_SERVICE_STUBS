package com.crio.zcommerce.backend.order.clients;

import com.crio.zcommerce.backend.order.config.AppConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import okhttp3.Headers;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Log4j2
public class UserClient {

  private static final String USER_API = "api/v1/user";

  private Headers headers;

  private final AppConfig appConfig;

  private final NetworkClient networkClient;
}

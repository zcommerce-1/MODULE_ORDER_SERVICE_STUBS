package com.crio.zcommerce.backend.order.controller;

import com.crio.zcommerce.backend.order.common.api.ApiResponse;
import com.crio.zcommerce.backend.order.common.api.ApiResponseStatus;
import com.crio.zcommerce.backend.order.common.exceptions.ApiException;
import com.crio.zcommerce.backend.order.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.order.controller.exchange.responses.CartResponse;
import com.crio.zcommerce.backend.order.model.Cart;
import com.crio.zcommerce.backend.order.model.ItemQuantity;
import com.crio.zcommerce.backend.order.service.CartService;
import jakarta.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.metrics.annotation.Timed;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Timed
@RestController
@RequiredArgsConstructor
@RequestMapping(CartController.CART_API_PREFIX)
public class CartController {

  public static final String CART_API_PREFIX = "/api/v1/cart";

  private static final String ID_ENDPOINT = "/{id:\\d+}";

  private static final String VIEW_ENDPOINT = "/view";

  private final CartService cartService;

  /**
   * URL: /api/v1/cart
   * Description: Get cart details of the user.
   * Method: GET
   *
   * @return cart details
   */
  @GetMapping
  @PreAuthorize("hasAuthority('ROLE_USER')")
  public ResponseEntity<ApiResponse<Cart>> getCartDetails(Principal principal) {
    ApiResponse<Cart> response = new ApiResponse<>();
    String userId = principal.getName();
    try {
      Cart cart = cartService.getCartDetails(userId);
      response.setData(cart);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(
          String.format("cart details not found for the given userId : %s", userId));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/cart/view
   * Description: Get cart details of the user.
   * Method: GET
   *
   * @return cart details
   */
  @GetMapping(VIEW_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_USER')")
  public ResponseEntity<ApiResponse<CartResponse>> getUserCartDetails(Principal principal) {
    ApiResponse<CartResponse> response = new ApiResponse<>();
    String userId = principal.getName();
    try {
      CartResponse cart = cartService.getUserCartDetails(userId);
      response.setData(cart);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(
          String.format("cart details not found for the given userId : %s", userId));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    } catch (IOException | ApiException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/cart
   * Description: Create cart
   * Method: POST
   *
   * @param cart of type Cart
   * @return Created Cart details
   */
  @PostMapping
  @PreAuthorize("hasAuthority('ROLE_USER')")
  public ResponseEntity<ApiResponse<Cart>> createCart(
      @RequestBody @Valid Cart cart, Principal principal) {
    String userId = principal.getName();
    cart.setUserId(principal.getName());
    // Creating cart for the user
    Cart createdCart = cartService.createCart(cart);
    ApiResponse<Cart> response = new ApiResponse<>();
    response.setData(createdCart);
    response.setStatus(ApiResponseStatus.SUCCESS);
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }

  /**
   * URL: /api/v1/cart/{id}
   * Description: Update cart details
   * Method: PUT
   *
   * @param cart of type Cart
   * @return updated cart details
   */
  @PutMapping(ID_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  public ResponseEntity<ApiResponse<Cart>> updateCart(
      @PathVariable("id") Long cartId, @RequestBody @Valid Cart cart) {
    ApiResponse<Cart> response = new ApiResponse<>();
    try {
      Cart updatedCart = cartService.updateCart(cartId, cart);
      response.setData(updatedCart);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(
          String.format(
              "Update cart failed as cart Details not found for the given cartId : %s", cartId));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/cart/
   * Description: Add Item in Cart
   * Method: PATCH
   *
   * @param itemQuantity of type ItemQuantity
   * @return Cart
   */
  @PatchMapping()
  @PreAuthorize("hasAuthority('ROLE_USER')")
  public ResponseEntity<ApiResponse<Cart>> updateItemInCart(
      @RequestBody @Valid ItemQuantity itemQuantity, Principal principal) {
    ApiResponse<Cart> response = new ApiResponse<>();
    String userId = principal.getName();
    try {
      Cart cart = cartService.updateItemInCart(userId, itemQuantity);
      response.setData(cart);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException | ApiException | IOException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/order/{id}
   * Description: Delete cart of given cart id
   * Method: DELETE
   */
  @DeleteMapping(ID_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  public ResponseEntity<ApiResponse<String>> deleteCart(@PathVariable("id") Long cartId) {
    cartService.deleteCart(cartId);
    ApiResponse<String> response = new ApiResponse<>();
    response.setStatus(ApiResponseStatus.SUCCESS);
    response.setMessage("Cart deleted");
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }
}

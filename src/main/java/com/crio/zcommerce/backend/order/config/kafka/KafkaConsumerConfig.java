package com.crio.zcommerce.backend.order.config.kafka;

import lombok.Data;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.DefaultErrorHandler;
import org.springframework.kafka.support.ExponentialBackOffWithMaxRetries;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
@Data
public class KafkaConsumerConfig {
  @Value("${kafka.retry.initial-delay}")
  private Long retryInitDelay;

  @Value("${kafka.retry.max-attempts}")
  private int maxRetryAttempts;

  @Autowired private KafkaTopicConfig kafkaTopicConfig;

  @Bean
  public ConsumerFactory<String, String> consumerFactory() {
    Map<String, Object> configProps = new HashMap<>();
    configProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaTopicConfig.getBootstrapAddress());
    configProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    configProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    return new DefaultKafkaConsumerFactory<>(configProps);
  }

  @Bean
  public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
    ConcurrentKafkaListenerContainerFactory<String, String> containerFactory =
            new ConcurrentKafkaListenerContainerFactory<>();
    containerFactory.setConsumerFactory(consumerFactory());
    containerFactory.setCommonErrorHandler(defaultErrorHandler());
    containerFactory.getContainerProperties().setAckMode(ContainerProperties.AckMode.RECORD);
    containerFactory.afterPropertiesSet();
    return containerFactory;
  }

  @Bean
  public DefaultErrorHandler defaultErrorHandler() {
    ExponentialBackOffWithMaxRetries backOff = new ExponentialBackOffWithMaxRetries(maxRetryAttempts);
    backOff.setInitialInterval(retryInitDelay);
    backOff.setMultiplier(2);
    DefaultErrorHandler errorHandler = new DefaultErrorHandler(backOff);
    errorHandler.addNotRetryableExceptions(NullPointerException.class);
    return errorHandler;
  }
}

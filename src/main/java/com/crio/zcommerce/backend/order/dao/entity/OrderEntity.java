package com.crio.zcommerce.backend.order.dao.entity;

import com.crio.zcommerce.backend.order.model.ItemQuantity;
import com.crio.zcommerce.backend.order.model.enums.OrderStatus;
import jakarta.persistence.*;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@SuperBuilder
@Entity
@Table(
    name = "orders",
    indexes = {
      @Index(name = "IDX_userId", columnList = "userId"),
      @Index(name = "IDX_orderId_userId", columnList = "id,userId"),
      @Index(name = "IDX_orderStatus", columnList = "status"),
    })
public class OrderEntity extends BaseEntity {

  @Column(nullable = false)
  private String userId;

  @Column(nullable = false)
  @ElementCollection
  @CollectionTable(name = "order_item_details", joinColumns = @JoinColumn(name = "order_id"))
  private List<ItemQuantity> itemQuantityList;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private OrderStatus status;

  @Column(nullable = false)
  private Double amount;

  @OneToMany(mappedBy = "order")
  @ToString.Exclude
  private Set<PaymentEntity> paymentList = new LinkedHashSet<>();

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "tracking_id", referencedColumnName = "id")
  private TrackingEntity tracking;

  /*
  note: below address fields should ideally go to diff table under a user,
  but for the simplicity we have kept here
  */
  @Lob
  @Column(nullable = false)
  private String shippingAddress;

  @Lob
  @Column(nullable = false)
  private String billingAddress;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    OrderEntity that = (OrderEntity) o;
    return getId() != null && Objects.equals(getId(), that.getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}

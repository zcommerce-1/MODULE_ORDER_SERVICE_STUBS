package com.crio.zcommerce.backend.order.utils.kafka.model;

import com.crio.zcommerce.backend.order.model.ItemQuantity;
import com.crio.zcommerce.backend.order.model.Order;
import com.crio.zcommerce.backend.order.model.enums.OrderStatus;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@Builder
public class OrderEvent implements ConsumerEvent {

  String userId;

  @NotNull private Order order;
}

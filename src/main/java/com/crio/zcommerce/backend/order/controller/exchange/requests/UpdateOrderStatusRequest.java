package com.crio.zcommerce.backend.order.controller.exchange.requests;

import com.crio.zcommerce.backend.order.model.enums.OrderStatus;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateOrderStatusRequest {

  @NotNull private OrderStatus orderStatus;
}

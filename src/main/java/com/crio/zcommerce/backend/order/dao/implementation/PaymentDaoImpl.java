package com.crio.zcommerce.backend.order.dao.implementation;

import com.crio.zcommerce.backend.order.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.order.controller.exchange.requests.UpdatePaymentRequest;
import com.crio.zcommerce.backend.order.dao.PaymentDao;
import com.crio.zcommerce.backend.order.dao.entity.PaymentEntity;
import com.crio.zcommerce.backend.order.dao.repository.PaymentRepository;
import com.crio.zcommerce.backend.order.mapper.OrderMapper;
import com.crio.zcommerce.backend.order.mapper.PaymentMapper;
import com.crio.zcommerce.backend.order.model.Payment;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
@RequiredArgsConstructor
public class PaymentDaoImpl implements PaymentDao {

  private final PaymentRepository paymentRepository;

  private final PaymentMapper paymentMapper;

  private final OrderMapper orderMapper;

  @Override
  @Transactional
  public Optional<Payment> findByPaymentId(Long paymentId) {
    // fetching for payment with payment
    Optional<PaymentEntity> paymentEntityOptional = paymentRepository.findById(paymentId);

    // check if optional is empty
    if (paymentEntityOptional.isPresent()) {
      Payment payment = paymentMapper.toDto(paymentEntityOptional.get());
      payment.setOrder(orderMapper.toDto(paymentEntityOptional.get().getOrder()));
      return Optional.of(payment);
    }

    // returning empty optional as payment is not found
    return Optional.empty();
  }

  @Override
  @Transactional
  public Payment createPayment(Payment payment) {
    PaymentEntity newPaymentEntity = paymentMapper.toEntity(payment);

    PaymentEntity paymentEntity = paymentRepository.save(newPaymentEntity);
    return paymentMapper.toDto(paymentEntity);
  }

  @Override
  @Transactional
  public Payment updatePayment(Long paymentId, UpdatePaymentRequest request)
      throws EntityDoesNotExistException {
    String logPrefix = "event : updatePayment, message: ";
    Optional<PaymentEntity> existingPaymentEntityOptional = paymentRepository.findById(paymentId);
    if (!existingPaymentEntityOptional.isPresent()) {
      String errorMessage =
          String.format("payment details not found for the paymentId : %s", paymentId);
      log.info(logPrefix + errorMessage);
      throw new EntityDoesNotExistException(errorMessage);
    }
    PaymentEntity existingPaymentEntity = existingPaymentEntityOptional.get();
    existingPaymentEntity.setTransactionId(request.getTransactionId());
    existingPaymentEntity.setPaymentStatus(request.getPaymentStatus());
    PaymentEntity updatedPaymentEntity = paymentRepository.save(existingPaymentEntity);
    Payment updatedPayment = paymentMapper.toDto(updatedPaymentEntity);
    updatedPayment.setOrder(orderMapper.toDto(updatedPaymentEntity.getOrder()));
    return updatedPayment;
  }

  @Override
  public void deletePayment(Long paymentId) {
    paymentRepository.deleteById(paymentId);
  }
}

package com.crio.zcommerce.backend.order.kafka.consumer;

import com.crio.zcommerce.backend.order.config.kafka.KafkaTopicConfig;
import com.crio.zcommerce.backend.order.model.Order;
import com.crio.zcommerce.backend.order.model.enums.WebhookTrigger;
import com.crio.zcommerce.backend.order.utils.kafka.KafkaUtil;
import com.crio.zcommerce.backend.order.utils.kafka.model.ConsumerEvent;
import com.crio.zcommerce.backend.order.utils.kafka.model.WebhookEvent;
import com.crio.zcommerce.backend.order.utils.kafka.model.OrderEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class OrderWebhookConsumer {
    private final ObjectMapper objectMapper;
    private final KafkaUtil kafkaUtil;
    private final KafkaTopicConfig kafkaTopicConfig;

    private final String LISTENER_LOG_PREFIX = "event: OrderWebhookConsumer.listen, message: ";

    @KafkaListener(topics = "#{kafkaTopicConfig.orderEventTopic}",
            groupId = "#{kafkaTopicConfig.orderWebhookConsumerGroup}",
            containerFactory = "kafkaListenerContainerFactory")
    public void listen(String message) {
        try {
            log.info(LISTENER_LOG_PREFIX + "message consumed from Kafka: {}", message);
            OrderEvent event = objectMapper.readValue(message, OrderEvent.class);

            // send webhook event
            ConsumerEvent consumerEvent =
                    WebhookEvent.builder()
                            .payload(event.getOrder())
                            .trigger(WebhookTrigger.NEW_ORDER.name())
                            .userId(event.getOrder().getUserId())
                            .build();

            kafkaUtil.sendEvent(consumerEvent, kafkaTopicConfig.getWebhookTriggerTopic());
            log.info(
                    LISTENER_LOG_PREFIX + "sent event to order topic userId : {} , orderId : {}",
                    event.getOrder().getUserId(),
                    event.getOrder().getId());
        } catch (JsonProcessingException ex) {
            log.error(LISTENER_LOG_PREFIX + "JsonProcessingException thrown. Consumed message: {}",
                    message);
        } catch (Exception ex) {
            log.error(LISTENER_LOG_PREFIX + "Exception thrown. msg={}", ex.getMessage());
            throw ex;
        }
    }
}
